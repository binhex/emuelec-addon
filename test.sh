#!/bin/bash

# update pacman database
pacman -Syy

# install required prereq packages
pacman -S zip perl-xml-parser gperf xorg bc lzop perl-json rpcsvc-proto --noconfirm

# switch user and compile and build emuelec
su nobody -c 'mkdir -p /cache/tmp && \
  cd /cache/tmp && \
  git clone https://github.com/shantigilbert/EmuELEC.git EmuELEC && \
  cd EmuELEC/ && \
  PROJECT=Amlogic-ng && \
  ARCH=arm DISTRO=EmuELEC && \
  ./emuelec-addon.sh'